insert ignore into sec_group(groupname, groupdesc) values("MANAGERS", "This group contains all managers in the firm.");
insert ignore into sec_group(groupname, groupdesc) values("LABORERS", "This group contains all laborers in the firm.");
insert ignore into sec_group(groupname, groupdesc) values("CLERKS", "This group contains all clerks in the firm.");

insert ignore into sec_user(username, password) values("musah1", SHA2("passmusah", 256));
insert ignore into sec_user(username, password) values("kwame1", SHA2("passkwame", 256));
insert ignore into sec_user(username, password) values("jterry1", SHA2("passjohn", 256));
insert ignore into sec_user(username, password) values("amy1", SHA2("passamy", 256));

insert ignore into sec_user_groups(username, groupname) values("musah1", "MANAGERS");
insert ignore into sec_user_groups(username, groupname) values("musah1", "LABORERS");
insert ignore into sec_user_groups(username, groupname) values("musah1", "CLERKS");
insert ignore into sec_user_groups(username, groupname) values("kwame1", "LABORERS");
insert ignore into sec_user_groups(username, groupname) values("jterry1", "CLERKS");
insert ignore into sec_user_groups(username, groupname) values("amy1", "MANAGERS");
insert ignore into sec_user_groups(username, groupname) values("amy1", "LABORERS");
insert ignore into sec_user_groups(username, groupname) values("amy1", "CLERKS");

insert into department(departmentid, departmentdescription, departmentname) values("ARCH1", "In charge of producing architectural drawings and designs for all construction work", "Architecture");
insert into department(departmentid, departmentdescription, departmentname) values("RFN1", "In charge of providing all roofing material and laborers", "Roofing");
insert into department(departmentid, departmentdescription, departmentname) values("TLN1", "In charge of providing all tiling materials and laborers", "Tiling");
insert into department(departmentid, departmentdescription, departmentname) values("MG1", "Individuals who play managerial roles in the company", "Management");

insert into employee(id, firstname, lastname, role, hiredate, salary, department_id, username) values (1, "Musah", "Codjoe", "CEO", "1984-08-02", 200000, "MG1", "musah1");
insert into employee(id, firstname, lastname, role, hiredate, salary, department_id, username) values (2, "John", "Terry", "Secretary", "1992-05-22", 100000, "MG1", "jterry1");
insert into employee(id, firstname, lastname, role, hiredate, salary, department_id, username) values (3, "Amy", "Wilson", "Managing Director", "1990-04-04", 150000, "MG1", "amy1");
insert into employee(id, firstname, lastname, role, hiredate, salary, department_id, username) values (4, "Kwame", "Ohene", "Laborer", "2002-09-04", 70000, "RFN1", "kwame1");

insert into equipment(serialnumber, brandname, category, chassisnumber, enginenumber, manufacturedate, model, odometerreading, purchasedate, registrationnumber, type) values(256712, "McNeilus", "Tractor", 567345, 8354894, "1995-06-05", "M185", 120000, "1998-11-22", 453789, "Concrete Mixer");
insert into equipment(serialnumber, brandname, category, chassisnumber, enginenumber, manufacturedate, model, odometerreading, purchasedate, registrationnumber, type) values(2367472, "Caterpillar", "Tractor", 5566356, 895906, "1997-06-09", "C173895", 180000, "2001-10-20", 527076, "Bulldozer");
insert into equipment(serialnumber, brandname, category, chassisnumber, enginenumber, manufacturedate, model, odometerreading, purchasedate, registrationnumber, type) values(6379274, "Komatsu", "Tractor", 89366483, 9725547, "2001-08-02", "K7839956", 20000, "2009-11-22", 893690, "Crane");
insert into equipment(serialnumber, brandname, category, chassisnumber, enginenumber, manufacturedate, model, odometerreading, purchasedate, registrationnumber, type) values(16894536728, "Caterpillar", "Tractor", 15684, 243836, "1993-11-11", "C451027", 180000, "2001-11-20", 893657, "Telehandler");

insert into tool(serialnumber, brandname, category, model, type) values(67208, "Stihl", "Cutting", "S15", "Chainsaw");
insert into tool(serialnumber, brandname, category, model, type) values(145273, "Estwing", "Hammer", "E4562", "Ballpein");
insert into tool(serialnumber, brandname, category, model, type) values(4237, "Hakko", "Cutting", "CHP170", "Plier");
insert into tool(serialnumber, brandname, category, model, type) values(12345, "Narex", "Cutting", "N6783", "Chisel");

insert into maintenance(id, duedate, maintenancedone, reminderdate, equipment_id) values(1, "2017-05-22", 0, "2017-05-20", 256712);
insert into maintenance(id, duedate, maintenancedone, reminderdate, equipment_id) values(2, "2017-08-08", 0, "2017-08-03", 2367472);


