/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.sfuseini.ejb;

import edu.iit.sat.itmd4515.sfuseini.domain.Equipment;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Named;

/**
 *
 * @author shameemahfuseini-codjoe
 */
@Named
@Stateless
public class EquipmentService extends BaseService<Equipment> {

    /**
     *
     */
    public EquipmentService() {
        super(Equipment.class);
    }

    /**
     *
     * @param serialNumber
     * @return
     */
    public Equipment findByEquipmentId(int serialNumber) {
        return getEntityManager()
                .createNamedQuery("Equipment.findByEquipmentId", Equipment.class)
                .setParameter("serialNumber", serialNumber)
                .getSingleResult();
    }

    /**
     *
     * @return
     */
    @Override
    public List<Equipment> findAll() {
        return getEntityManager().createNamedQuery("Equipment.findAll", Equipment.class).getResultList();
    }

    /**
     *
     * @param jsfEquipment
     */
    @Override
    public void update(Equipment jsfEquipment) {
        Equipment oldEquipment = getEntityManager().find(Equipment.class, jsfEquipment.getSerialNumber());

        jsfEquipment.setSerialNumber(oldEquipment.getSerialNumber());
        getEntityManager().merge(jsfEquipment);
    }

    /**
     *
     * @param equipment
     */
    @Override
    public void create(Equipment equipment) {
        super.create(equipment); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *
     * @param e
     */
    @Override
    public void remove(Equipment e) {
        super.remove(e); //To change body of generated methods, choose Tools | Templates.
    }
}
